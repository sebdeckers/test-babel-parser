const parser = require('@babel/parser')
const source = `
  export { f }
  function f () {}
`
const options = {
  sourceType: 'module',
  plugins: ['exportDefaultFrom']
}

parser.parse(source, { ...options, strictMode: true })
// OK

parser.parse(source, { ...options, strictMode: false })
// SyntaxError: Export 'f' is not defined (2:11)
